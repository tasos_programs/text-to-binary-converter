## Binary to Text/Text to Binary Converter
A simple program that converts binary characters to text or text characters to binary.
## Example

> ``1101000 1100101 1101100 1101100 1101111 00100000 1110100 1101000
> 1100101 1110010 1100101 100001``

converts to

> ``hello there!``
## Requirements
This program requires that you have [Java](https://www.java.com/en/) installed.
You have to run this program from a **terminal** session. (cmd for Windows or your favorite terminal app on Linux/MacOS)
## How to use 
- Both ``to_words.jar`` and ``to_binary.jar`` have the same usage.
- To convert **a string of text to binary**, use ``java -jar to_binary.jar "Your text here"`` (without quotes)
- To convert **binary integers to a string of text**, use ``java -jar to_words.jar "Your 8bit binary numbers here"``(without quotes)
## Limitations
- Your  8bit binary numbers should have space between each other. (``1101000 1100101`` and not ``11010001100101``)
- To compile from source, put the source code files inside a folder named "Text_To_Binary"
