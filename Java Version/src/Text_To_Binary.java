package Text_To_Binary;

public class Text_To_Binary {

    private static String ToBinary(String word) {
        StringBuilder result = new StringBuilder();
        for (char c : word.toCharArray()) {
            result.append(Integer.toBinaryString(c) + " "); //for every char in our string, convert to binary and add a space
        }
        return result.toString();
    }

    private static void CheckArgs(int length) {
        // If user didn't put any words as arguments, exit the program
        if (length < 1) {
            System.out.println("You must put words!\nUse 'java -jar program.jar WORDS' to run!");
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        CheckArgs(args.length);

        StringBuilder result = new StringBuilder();
        final String space = "00100000 ";

        for (String arg : args) {
            result.append(ToBinary(arg) + ( arg != args[args.length-1 ] ? space : "")); // append the result of ToBinary, and add space. If last word, don't add space
        }

        System.out.println(result.toString());
    }
}
