package Text_To_Binary;

public class Binary_To_Text {

    private static void CheckArgs(int length) {
        // If user didn't put any words as arguments, exit the program
        if (length < 1) {
            System.out.println("You must put words!\nUse 'java -jar program.jar 8bit DECIMAL NUMBERS' to run!");
            System.exit(-1);
        }
    }

    private static String To_Words(String word) {
        StringBuilder result = new StringBuilder();
        result.append((char) Integer.parseInt(word, 2)); // appends to result char by char the actual word
        return result.toString();
    }

    public static void main(String[] args) {
        CheckArgs(args.length); // exit if no words given
        StringBuilder result = new StringBuilder();

        for (String word : args) {
            result.append(To_Words(word)); // append the words representation of the binary string given
        }
        
        System.out.println(result.toString());
    }
}
