#include <iostream>
#include <string>
#include <bitset>

std::string ConvertToBinary(std::string word)
{
    std::string toBinary;
    for (char c : word)
    {
        // Generate an 8bit bitset from the character, and append it to our toBinary string
        std::bitset<8> character(c);
        toBinary += character.to_string() + " ";
    }
    return toBinary;
}

void CheckArgs(int argc, char *programName)
{
    // Exit the program if user didn't put any words as arguments
    if (argc < 2)
    {
        printf("You must put words!\nUse '%s words' to run.\n", programName);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[])
{
    CheckArgs(argc, argv[0]); // exit if no words found

    std::string toBinary;
    const std::string space = "00100000 ";

    for (int i = 1; i <= argc - 1; i++)
    {
        // append the result of ToBinary, and add space. If last word, don't add a space
        toBinary += ConvertToBinary(argv[i]) + (i != argc - 1 ? space : "");
    }
    std::cout << toBinary << std::endl;
}
