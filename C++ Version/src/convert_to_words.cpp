#include <iostream>
#include <string>
#include <sstream>
#include <bitset>
#include <vector>

std::string ToWords(std::string word)
{
    // Converts an 8bit number to a character using std::bitset
    std::stringstream fromBinary(word);
    std::stringstream toWords;

    std::bitset<8> bits; // Create the bitset
    fromBinary >> bits; // Put the string representation of our binary numbers into the bitset
    toWords << char(bits.to_ulong()); // Convert and put it into toWords

    return toWords.str();
}

void CheckArgs(int argc, char *programName)
{
    // Exit the program if user didn't put any words as arguments
    if (argc < 2)
    {
        printf("You must put words!\nUse '%s words' to run.\n", programName);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[])
{
    CheckArgs(argc, argv[0]); // exit if no words found

    std::string toWords;
    std::vector<std::string> argList(argv + 1, argv + argc); // convert args to a vector for easy looping

    for (int i = 0; i < argList.size(); i++)
    {
        toWords += ToWords(argList[i]); // append the words representation of the binary string given
    }

    std::cout << toWords << std::endl;
}
