## Binary to Text/Text to Binary Converter
A simple program that converts binary characters to text or text characters to binary.
## Example

> ``1101000 1100101 1101100 1101100 1101111 00100000 1110100 1101000
> 1100101 1110010 1100101 100001``

converts to

> ``hello there!``
## Requirements
You have to run this program from a **terminal** session. (cmd for Windows or your favorite terminal app on Linux/MacOS)
## How to use 
- Both ``to_words.out`` and ``to_binary.out`` have the same usage.
- To convert **a string of text to binary**, use ``./to_binary.out "Your text here"`` (without quotes)
- To convert **binary integers to a string of text**, use ``./to_words.out "Your 8bit binary numbers here"``(without quotes)
## Limitations
- Your  8bit binary numbers should have space between each other. (``1101000 1100101`` and not ``11010001100101``)
- You need to compile it from source if you want to run this on a Windows machine.

